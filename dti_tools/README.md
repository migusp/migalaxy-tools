#Diffusion Tensor Imaging

This package contains a set of tools for DTI exploration and processing.


##Brain Extraction

For a given patient, this tool will create a brain extraction mask and produce an output NIfTI. B-values
and B-vectors will remain unaltered and will be copied to the new output dataset.


##Motion Correction of NIfTI files

For a given patient, this tool will estimate motion parameters, average T2, rebuild data and adjust B-values and B-vectors.


##Eddy Correction

For a given patient, this tool will estimate the eddy correction on the input and produce an output NIfTI. B-values
and B-vectors will remain unaltered and will be copied to the new output dataset.


##Tensor Estimation

For a given patient, this tool will extract DTI maps from a NIfTI file, producing multiple outputs that represents each DTI tensor map.
