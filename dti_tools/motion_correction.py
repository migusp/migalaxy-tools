#!/usr/bin/env python
# Author: topcin@ime.usp.br

# Original shell script:
# Estimateing motion parameters for assuarance
# 3dvolreg -verbose -Fourier -prefix MC_${FN} -base 0 -dfile MC_${FN}.txt ${FN}.nii.gz

# Averaging T2, rebuilding data and ajusting .bvs
# 3dTstat -mean -prefix meanT2_${FN} MC_${FN}+orig[0..3]
# 3dTcat -prefix MC_T232dir_${FN} meanT2_${FN}+orig MC_${FN}+orig[4..35]
# 1d_tool.py -infile ${FN}.bvec[3..35] -write ${FN}_32dir.bvec
# 1d_tool.py -infile ${FN}.bval[3..35] -write ${FN}_32dir.bval
# 3dAFNItoNIFTI -prefix MC_T232dir_${FN}.nii.gz MC_T232dir_${FN}+orig
# rm *+orig* ${FN}.bvec ${FN}.bval ${FN}.nii.gz

import sys
import os
import os.path
import subprocess
import optparse
import shutil
import glob
import tempfile

def _stop_err(msg):
    sys.stderr.write("%s\n" % msg)
    sys.exit()

def _copy(source, destination):
    try:
        os.link(source, destination)
        print 'link src: %s; dest: %s' % (source, destination)
    except:
        shutil.copy(source, destination)
        print 'copy src: %s; dest: %s' % (source, destination)

def main():
    parser = optparse.OptionParser()

    parser.add_option('', '--input-dir', action="store", type="string", dest='input_dir', help='Input nii.gz/bval/bvec folder')
    parser.add_option('', '--output-dir', action="store", type="string", dest='output_dir', help='Output nii.gz/bval/bvec folder')
    parser.add_option('-p', '--prefix', action="store", type="string", dest='prefix', help='Prefix for generated filenames (optional)')

    (opt, args) = parser.parse_args()

    working_dir = os.getcwd()
    input_nii_gz = os.path.join(opt.input_dir, 'dataset.nii.gz')
    input_bvec = os.path.join(opt.input_dir, 'dataset.bvec')
    input_bval = os.path.join(opt.input_dir, 'dataset.bval')

    output_nii_gz = os.path.join(opt.output_dir, 'dataset.nii.gz')
    output_bvec = os.path.join(opt.output_dir, 'dataset.bvec')
    output_bval = os.path.join(opt.output_dir, 'dataset.bval')

    if not os.path.exists(opt.output_dir):
        os.makedirs(opt.output_dir)

    print '\nargs:\n%s' % '\n'.join(sys.argv[1:])
    print '\noptions (a):\n%s' % opt
    print '\nworking_dir: %s' % working_dir

    if not os.path.isfile(input_nii_gz):
        _stop_err('No valid input NIfTI file was given')

    #TODO: Pass the ranges [0..3], [4..35], etc... as parameters in the tool.
    # 3dvolreg -verbose -Fourier -prefix MC -base 0 -dfile MC.txt ${FN}.nii.gz
    cmd = '3dvolreg -verbose -Fourier -prefix MC_{prefix} -base 0 -dfile MC_{prefix}.txt {input_nii_gz};\n'\
        .format(prefix=opt.prefix, input_nii_gz=input_nii_gz)

    # 3dTstat -mean -prefix meanT2_${FN} MC_${FN}+orig[0..3]
    cmd += '3dTstat -mean -prefix meanT2_{prefix} MC_{prefix}+orig[0..3];\n'\
        .format(prefix=opt.prefix)

    # 3dTcat -prefix MC_T232dir_${FN} meanT2_${FN}+orig MC_${FN}+orig[4..35]
    cmd += '3dTcat -prefix MC_T232dir_{prefix} meanT2_{prefix}+orig MC_{prefix}+orig[4..35];\n'\
        .format(prefix=opt.prefix)

    # 1d_tool.py -infile ${FN}.bvec[3..35] -write ${FN}_32dir.bvec
    cmd += '1d_tool.py -infile {input_bvec}[3..35] -write {output_bvec};\n'\
        .format(input_bvec=input_bvec, output_bvec=output_bvec)

    # 1d_tool.py -infile ${FN}.bval[3..35] -write ${FN}_32dir.bval
    cmd += '1d_tool.py -infile {input_bval}[3..35] -write {output_bval};\n'\
        .format(input_bval=input_bval, output_bval=output_bval)

    # 3dAFNItoNIFTI -prefix MC_T232dir_${FN}.nii.gz MC_T232dir_${FN}+orig
    cmd += '3dAFNItoNIFTI -prefix {output_nii_gz} MC_T232dir_{prefix}+orig;\n'\
        .format(output_nii_gz=output_nii_gz, prefix=opt.prefix)

    print 'running: %s' % cmd

    try:
        with tempfile.NamedTemporaryFile(prefix='log.mc.', suffix='.out', dir=working_dir, delete=False) as tmp_stdout,\
                tempfile.NamedTemporaryFile(prefix='log.mc.', suffix='.err', dir=working_dir, delete=False) as tmp_stderr:
            proc = subprocess.Popen(cmd, shell=True, cwd='.', stdout=tmp_stdout, stderr=tmp_stderr)
            returncode = proc.wait()

            tmp_stdout.seek(0)
            print tmp_stdout.read()

            if returncode:
                try:
                    tmp_stderr.seek(0)
                    raise Exception, tmp_stderr.read()
                except OverflowError:
                    pass
    except:
        _stop_err('Child process failed: %s' % cmd)

if __name__ == '__main__': main()