#!/bin/bash

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

if [ -z "$FREESURFER_HOME" ]; then
    # FreeSurfer needs to exist at the following location in the execute machine (local or cluster)
    echo "Assuming FreeSurfer is installed in /usr/local/freesurfer"
    export FREESURFER_HOME=/usr/local/freesurfer
fi

export SUBJECTS_DIR=$FREESURFER_HOME/subjects
export MNI_DIR=$FREESURFER_HOME/mni
export FSFAST_HOME=$FREESURFER_HOME/fsfast
export MINC_BIN_DIR=$FREESURFER_HOME/mni/bin
export MINC_LIB_DIR=$FREESURFER_HOME/mni/lib
export MNI_DATAPATH=$FREESURFER_HOME/mni/data
export FMRI_ANALYSIS_DIR=$FREESURFER_HOME/fsfast
export PERL5LIB=$FREESURFER_HOME/mni/lib/perl5/5.8.5
export MNI_PERL5LIB=$FREESURFER_HOME/mni/lib/perl5/5.8.5

source $FREESURFER_HOME/SetUpFreeSurfer.sh
