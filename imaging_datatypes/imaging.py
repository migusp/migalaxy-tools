import gzip
import binascii
import os
from urllib import urlencode
import zipfile

from galaxy.datatypes import data
from galaxy.datatypes.data import Data
from galaxy.datatypes.binary import Binary
from galaxy.datatypes.binary import CompressedZipArchive
from galaxy.datatypes.images import Html
from galaxy.datatypes.metadata import MetadataElement
from galaxy.datatypes.util.image_util import *

import dicom as dcm
import nibabel as nib

log = logging.getLogger(__name__)


class FreeSurferSubject(Binary):
    """Class describing a FreeSurfer zip that encapsulates the content of FreeSurfer recon-all subject output"""
    file_ext = "fssub"
    is_binary = True
    compressed = True
    allow_datatype_change = False
    composite_type ='basic'

    MetadataElement(name="Modality", desc="Modality", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="Manufacturer", desc="Manufacturer", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="ManufacturerModelName", desc="Manufacturer Model Name", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="Modality", desc="Modality", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="AccessionNumber", desc="Accession Number", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="AcquisitionDate", desc="Acquisition Date", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="PatientID", desc="Patient ID", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="PatientName", desc="Patient Name", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="SeriesNumber", desc="Series Number", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="StudyID", desc="Study ID", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)

    def init_meta(self, dataset, copy_from=None):
        Data.init_meta(self, dataset, copy_from=copy_from)
        if copy_from is not None:
            patient_id = copy_from.name
            if hasattr(copy_from.metadata, 'PatientID') and copy_from.metadata.PatientID:
                patient_id = copy_from.metadata.Modality
            dataset.metadata.PatientID = patient_id.split()[0]


Binary.register_unsniffable_binary_ext("fssub")

class Dicom(data.Data):
    """Class describing a DICOM zip containing directories and DICOM files"""
    file_ext = "dcm.zip"
    is_binary = True
    compressed = True
    allow_datatype_change = False

    MetadataElement(name="Modality", desc="Modality", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="Manufacturer", desc="Manufacturer", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="ManufacturerModelName", desc="Manufacturer Model Name", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="Modality", desc="Modality", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="AccessionNumber", desc="Accession Number", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="AcquisitionDate", desc="Acquisition Date", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="PatientID", desc="Patient ID", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="PatientName", desc="Patient Name", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="SeriesNumber", desc="Series Number", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="StudyID", desc="Study ID", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)

    def set_peek(self, dataset, is_multi_byte=False):
        if not dataset.dataset.purged:
            peek = 'DICOM'
            if dataset.metadata.Modality is not None:
                peek += ' ({modality})'.format(
                    modality=dataset.metadata.Modality)
            if dataset.metadata.PatientName is not None:
                peek += ' {patient_name}'.format(
                    patient_name=dataset.metadata.PatientName)
            dataset.peek = peek
        else:
            dataset.peek = 'file does not exist'
            dataset.blurb = 'file purged from disk'

    def sniff(self, filename):
        if not zipfile.is_zipfile(filename):
            return False
        with zipfile.ZipFile(filename, "r") as zip_file:
            for i, info in enumerate(zip_file.filelist):
                if i > 5:
                    return False
                with zip_file.open(info) as file:
                    log.info("Sniffing zipped file {}/{}".format(filename, info.filename))
                    file.read(0x80) # preamble
                    if file.read(4) == b"DICM":
                        return True;

    def display_peek(self, dataset):
        try:
            return dataset.peek
        except:
            return "DICOM file (%s)" % (data.nice_size(dataset.get_size()))

    def extract_all(self, dataset):
        if not zipfile.is_zipfile(dataset.file_name):
            return

        if not os.path.exists(dataset.extra_files_path):
            os.makedirs(dataset.extra_files_path)
            with zipfile.ZipFile(dataset.file_name, "r") as zip_file:
                zip_file.extractall(dataset.extra_files_path)

    def set_meta(self, dataset, overwrite=True, **kwd):
        self.extract_all(dataset)

        if os.path.isdir(dataset.extra_files_path) and dcm is not None:
            metadata = [ meta_name for meta_name, meta_spec in dataset.datatype.metadata_spec.iteritems() ]
            print "Trying to get %s" % metadata

            for root, _dirs, files in os.walk(dataset.extra_files_path):
                for file in files:
                    print "Analysing file %s" % os.path.join(root, file)
                    try:
                        ds = dcm.read_file(os.path.join(root, file))
                        for meta_name in metadata:
                            meta_value = ds.get(meta_name)
                            meta_value = '' if meta_value is None else str(meta_value)
                            if meta_value:
                                setattr(dataset.metadata, meta_name, meta_value)
                                metadata.remove(meta_name)
                                print "found metadata %s" % meta_name

                        if not metadata:
                            break
                    except:
                        print "no metadata here :("
                        pass

    def get_mime(self):
        return 'application/zip'

Binary.register_sniffable_binary_format("dcm.zip", "dcm.zip", Dicom)
Binary.register_unsniffable_binary_ext("dcm.zip")

class Nifti(Binary):
    file_ext = "nii.gz"
    is_binary = True
    compressed = True
    allow_datatype_change = False

    MetadataElement(name="Modality", desc="Modality", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="Manufacturer", desc="Manufacturer", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="ManufacturerModelName", desc="Manufacturer Model Name", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="Modality", desc="Modality", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="AccessionNumber", desc="Accession Number", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="AcquisitionDate", desc="Acquisition Date", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="PatientID", desc="Patient ID", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="PatientName", desc="Patient Name", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="SeriesNumber", desc="Series Number", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)
    MetadataElement(name="StudyID", desc="Study ID", default=None, set_in_upload=True, readonly=False, visible=True, optional=True)

    def get_mime(self):
        return 'application/octet-stream'

    def init_meta(self, dataset, copy_from=None):
        Data.init_meta(self, dataset, copy_from=copy_from)
        if copy_from is not None:
            base_name = ''
            if hasattr(copy_from.metadata, 'Modality'):
                base_name += copy_from.metadata.Modality
            if hasattr(copy_from.metadata, 'PatientName'):
                base_name += copy_from.metadata.PatientName
            if base_name:
                dataset.metadata.base_name = base_name

    def sniff(self, filename):
        if nib is not None:
            try:
                nib.load(filename)
                return True
            except:
                return False
        else:
            try:
                header = gzip.open(filename).read(4)
                if binascii.b2a_hex(header) == binascii.hexlify('\x5c\x01\x00\x00'):
                    return True
                return False
            except:
                return False

    def set_peek(self, dataset, is_multi_byte=False):
        if not dataset.dataset.purged:
            peek = 'NIfTI'
            if dataset.metadata.Modality is not None:
                peek += ' ({modality})'.format(
                    modality=dataset.metadata.Modality)
            if dataset.metadata.PatientName is not None:
                peek += ' {pacient_name}'.format(
                    pacient_name=dataset.metadata.PatientName)
            dataset.peek = peek
            dataset.blurb = data.nice_size(dataset.get_size())
        else:
            dataset.peek = 'file does not exist'
            dataset.blurb = 'file purged from disk'

    def display_peek(self, dataset):
        try:
            return dataset.peek
        except:
            return "NIfTI file (%s)" % (data.nice_size(dataset.get_size()))

Binary.register_sniffable_binary_format("nii.gz", "nii.gz", Nifti)
Binary.register_unsniffable_binary_ext("nii.gz")
